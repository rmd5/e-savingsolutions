function openModal(id, section) {
    var modal = document.getElementById(id);
    var section = document.getElementById(section);
    if (modal && modal.style) {
        modal.style.display = 'block';
        if (section == "about"){
            document.getElementById('team').style.display = 'none';
            document.getElementById('beliefs').style.display = 'none';
            document.getElementById('projects').style.display = 'none';
            document.getElementById('contact').style.display = 'none';
        }
        if (section == "team"){
            document.getElementById('beliefs').style.display = 'none';
            document.getElementById('projects').style.display = 'none';
            document.getElementById('contact').style.display = 'none';
        }
        if (section =='beliefs'){
            document.getElementById('projects').style.display = 'none';
            document.getElementById('contact').style.display = 'none';
        }
        if (section == "projects") {
            document.getElementById('contact').style.display = 'none';
        }
    } 
}

function closeModal(id) {
    var modal = document.getElementById(id);
    if (modal && modal.style) {
        modal.style.display = 'none';
        document.getElementById('home').style.display = '';
        document.getElementById('about').style.display = '';
        document.getElementById('team').style.display = '';
        document.getElementById('beliefs').style.display = '';
        document.getElementById('projects').style.display = '';
        document.getElementById('contact').style.display = '';
    }
}

function normal() {
    document.body.style.backgroundColor = 'rgb(51, 51, 51)';
    document.getElementById('middle-wrap').style.backgroundImage = 'url("Images/backgroundWebSafe.svg")';
    document.getElementById('normal').style.fontWeight = '900';
    document.getElementById('protanopia').style.fontWeight = 'normal';
    document.getElementById('deuteranopia').style.fontWeight = 'normal';
    document.getElementById('tritanopia').style.fontWeight = 'normal';
    document.getElementById('monochrome').style.fontWeight = 'normal';

    document.getElementById('normal').style.textShadow = '0 0 0.01px black';
    document.getElementById('protanopia').style.textShadow = '';
    document.getElementById('deuteranopia').style.textShadow = '';
    document.getElementById('tritanopia').style.textShadow = '';
    document.getElementById('monochrome').style.textShadow = '';

    var modals = document.getElementsByClassName('modal');
    for (var i = 0; i < modals.length; i++) {
        modals[i].style.backgroundColor = 'rgba(0, 51, 0, 0.9)';
    }
}

function protanopia() {
    document.body.style.backgroundColor = 'rgb(0, 73, 73)';
    document.getElementById('middle-wrap').style.backgroundImage = 'url("Images/backgroundProtanopia.svg")';
    document.getElementById('normal').style.fontWeight = 'normal';
    document.getElementById('protanopia').style.fontWeight = '900';        
    document.getElementById('deuteranopia').style.fontWeight = 'normal';
    document.getElementById('tritanopia').style.fontWeight = 'normal';
    document.getElementById('monochrome').style.fontWeight = 'normal';

    document.getElementById('normal').style.textShadow = '';
    document.getElementById('protanopia').style.textShadow = '0 0 0.01px black';
    document.getElementById('deuteranopia').style.textShadow = '';
    document.getElementById('tritanopia').style.textShadow = '';
    document.getElementById('monochrome').style.textShadow = '';

    var modals = document.getElementsByClassName('modal');
    for (var i = 0; i < modals.length; i++) {
        modals[i].style.backgroundColor = 'rgba(73, 0, 0, 0.9)';
    }
}

function deuteranopia() {
    document.body.style.backgroundColor = 'rgb(0, 73, 73)';
    document.getElementById('middle-wrap').style.backgroundImage = 'url("Images/backgroundDeuteranopia.svg")';
    document.getElementById('normal').style.fontWeight = 'normal';
    document.getElementById('protanopia').style.fontWeight = 'normal';        
    document.getElementById('deuteranopia').style.fontWeight = '900';
    document.getElementById('tritanopia').style.fontWeight = 'normal';
    document.getElementById('monochrome').style.fontWeight = 'normal';

    document.getElementById('normal').style.textShadow = '';
    document.getElementById('protanopia').style.textShadow = '';
    document.getElementById('deuteranopia').style.textShadow = '0 0 0.01px black';
    document.getElementById('tritanopia').style.textShadow = '';
    document.getElementById('monochrome').style.textShadow = '';

    var modals = document.getElementsByClassName('modal');
    for (var i = 0; i < modals.length; i++) {
        modals[i].style.backgroundColor = 'rgba(37, 0, 74, 0.9)';
    }
}

function tritanopia() {
    document.body.style.backgroundColor = 'rgb(73, 0, 146)';
    document.getElementById('middle-wrap').style.backgroundImage = 'url("Images/backgroundTritanopia.svg")';
    document.getElementById('normal').style.fontWeight = 'normal';
    document.getElementById('protanopia').style.fontWeight = 'normal';        
    document.getElementById('deuteranopia').style.fontWeight = 'normal';
    document.getElementById('tritanopia').style.fontWeight = '900';
    document.getElementById('monochrome').style.fontWeight = 'normal';

    document.getElementById('normal').style.textShadow = '';
    document.getElementById('protanopia').style.textShadow = '';
    document.getElementById('deuteranopia').style.textShadow = '';
    document.getElementById('tritanopia').style.textShadow = '0 0 0.01px black';
    document.getElementById('monochrome').style.textShadow = '';

    var modals = document.getElementsByClassName('modal');
    for (var i = 0; i < modals.length; i++) {
        modals[i].style.backgroundColor = 'rgba(73, 0, 0, 0.9)';
    }
}

function monochrome() {
    document.body.style.backgroundColor = 'rgb(51, 51, 51)';
    document.getElementById('middle-wrap').style.backgroundImage = 'url("Images/backgroundMonochrome.svg")';
    document.getElementById('normal').style.fontWeight = 'normal';
    document.getElementById('protanopia').style.fontWeight = 'normal';        
    document.getElementById('deuteranopia').style.fontWeight = 'normal';
    document.getElementById('tritanopia').style.fontWeight = 'normal';
    document.getElementById('monochrome').style.fontWeight = '900';

    document.getElementById('normal').style.textShadow = '';
    document.getElementById('protanopia').style.textShadow = '';
    document.getElementById('deuteranopia').style.textShadow = '';
    document.getElementById('tritanopia').style.textShadow = '';
    document.getElementById('monochrome').style.textShadow = '0 0 0.01px black';

    var modals = document.getElementsByClassName('modal');
    for (var i = 0; i < modals.length; i++) {
        modals[i].style.backgroundColor = 'rgba(51, 51, 51, 0.9)';
    }
}